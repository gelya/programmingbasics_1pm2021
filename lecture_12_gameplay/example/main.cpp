﻿// Подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <bird.hpp>
#include <iostream>
#include <pig.hpp>

//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

using namespace std::chrono_literals;

int main()
{
    float t = 0;
    float sx = 313;
    float sy = 400;
    int score = 0;

    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(626, 442), "Angry Birds");

    // Подгрузка фонового изображения
    sf::Texture texture;
    if (!texture.loadFromFile("img/back.jpg"))
    {
        std::cout << "ERROR when loading back.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture);

    // Подгрузка шрифта и создание отображения счета
    sf::Font font;
    if (!font.loadFromFile("fonts/arial.ttf"))
    {
        std::cout << "ERROR: font was not loaded." << std::endl;
        return -1;
    }

    sf::Text text;
    text.setFont(font);
    text.setString("Hello world");
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::Red);

    // Добавление иконки
    sf::Image icon;
    if (!icon.loadFromFile("img/bird32.png"))
    {
        return -1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());

    // Генерация объектов
    std::vector<mt::Pig*> pigs;
    pigs.push_back(new mt::Pig(400, 400, 25));
    pigs.push_back(new mt::Pig(450, 400, 25));
    pigs.push_back(new mt::Pig(500, 400, 25));

    // Подгрузка картинок и завершение программы, если они не загрузились
    for (const auto& pig : pigs)
        if (!pig->Setup())
            return -1;

    mt::Bird* bird = nullptr;

    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            // Дребезг контактов
            //while (sf::Mouse::isButtonPressed(sf::Mouse::Left));
            sf::Vector2i mp = sf::Mouse::getPosition(window);

            float d = sqrt((mp.x - sx) * (mp.x - sx) + (mp.y - sy) * (mp.y - sy));
            float ay = mp.y - sy;
            float ax = mp.x - sx;

            float angle = acos(ax / sqrt(ax*ax + ay*ay));

            if (bird != nullptr)
                delete bird;

            bird = new mt::Bird(sx, sy, 20, angle, d);

            if (!bird->Setup())
            {
                delete bird;
                window.close();
                return -1;
            }

            t = 0;
        }

        // Движение птичка
        if (bird != nullptr)
        {
            bird->Move(t);

            // Проверка соударения птички и поросят
            for (int i = 0; i < pigs.size(); i++)
            {
                int X = bird->GetX();
                int Y = bird->GetY();
                float R = bird->GetR();

                int x = pigs[i]->GetX();
                int y = pigs[i]->GetY();
                float r = pigs[i]->GetR();

                float d = sqrt((X-x)*(X-x) + (Y-y)*(Y-y));

                if (R + r >= d)
                {
                    delete pigs[i];
                    pigs.erase(pigs.begin() + i);
                    i--;
                    score++;
                }
            }
        }

        // Вывод на экрна
        window.clear();

        // Вывод фона
        window.draw(back);

        // Вывод счета
        text.setString(std::string("Score ") + std::to_string(score));
        window.draw(text);

        for(const auto& pig : pigs)
            window.draw(*pig->Get());
        if(bird != nullptr)
            window.draw(*bird->Get());

        // Отобразить на окне все, что есть в буфере
        window.display();

        // https://ravesli.com/urok-129-tajming-koda-vremya-vypolneniya-programmy/
        std::this_thread::sleep_for(40ms);
        t += 0.04;
    }

    if (bird != nullptr)
        delete bird;

    return 0;
}